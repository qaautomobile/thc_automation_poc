package Executional;


import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import Functional.CommonFunctions;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class AddClient_From_ClientPage {

	WebDriver driver = Login_Application.driver;
	
	@Test(priority=1)
	public void Click_On_Intake() throws SQLException, InterruptedException
	{	
		driver = Login_Application.driver;
		
		driver.findElement(By.cssSelector("#TabIntake")).click();	
		Thread.sleep(2000);
	}
	
	@Test(priority=2)
	public void Click_ClientPage() throws InterruptedException
	{
		// Select Client from popup
		driver.findElement(By.cssSelector("#nav_conts")).click();
		Thread.sleep(4000);
	}
	
	@Test(priority=2)
	public void Open_ClientOptions() throws InterruptedException
	{
		driver.findElement(By.cssSelector("#Tabnav_conts")).click();	
		Thread.sleep(4000);
	}
	
	@Test(priority=3)
	public void Open_ClientPage() throws InterruptedException
	{
		driver.findElement(By.cssSelector("#secondTabActionListFlyoutControl_scrollableContainer .nav-section:nth-child(1) li:nth-child(1)")).click();
		Thread.sleep(25000);
	}

	@Test(priority=4)
	public void Click_NewButton() throws InterruptedException
	{
		WebElement NewButton = driver.findElement(By.xpath(".//*[@id='contact|NoRelationship|Form|Mscrm.Form.contact.NewRecord"));
		NewButton.click();
		Thread.sleep(25000);
//		NewButton.sendKeys(Keys.ENTER);
		
		try {
			   Robot robot = new Robot();
			   robot.keyPress(KeyEvent.VK_ENTER);
			   robot.keyRelease(KeyEvent.VK_ENTER);
			   robot.delay(200);
			}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
	}
	
	@Test(priority=5)
	public void Open_NewClientEntryPage() throws InterruptedException
	{
		// switch frame
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		driver.switchTo().frame(driver.findElement(By.id("contentIFrame1")));
		Thread.sleep(2000);
		driver.switchTo().frame(driver.findElement(By.id("WebResource_sectiontabs1")));
		Thread.sleep(2000);

		// open client details tab
		driver.findElement(By.xpath(".//*[@id='CLIENTDETAILS_TAB']/a")).click();
		Thread.sleep(5000);
		
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.id("contentIFrame1")));
		Thread.sleep(3000);
	}
	
	@Test(priority=6)
	public void NewClient_Fax() throws InterruptedException
	{
		WebElement faxnumber = driver.findElement(By.id("fax"));
		faxnumber.sendKeys("222666");
		Thread.sleep(2000);
		
		WebElement nextbutton = driver.findElement(By.id("invorg_bool_cansendcommunicationemail"));
		nextbutton.sendKeys(Keys.TAB);
	}
	
	@Test(priority=6)
	public void NewClinet_PrimaryLanguage_Other() throws InterruptedException
	{
		WebElement PLanguage = driver.findElement(By.id("invorg_os_primarycommunicationlanguage"));
		PLanguage.click();
		Thread.sleep(3000);
		
		Select SelectPLanguage = new Select(driver.findElement(By.id("invorg_os_primarycommunicationlanguage_i")));
		SelectPLanguage.selectByVisibleText("Other");
		Thread.sleep(3000);
	}
	
	@Test(priority=7)
	public void NewClinet_PrimaryLanguage_OtherTest() throws InterruptedException
	{
		WebElement PLanguage = driver.findElement(By.id("invorg_slot_primarycommunicationlanguageother"));
		PLanguage.sendKeys("TestLanguage");
		Thread.sleep(3000);
	}
	
	@Test(priority=8)
	public void NewClinet_CommunicationLanguage() throws InterruptedException
	{
		driver.findElement(By.id("invorg_os_secondarycommunicationlanguage")).sendKeys(Keys.TAB);
		Thread.sleep(1500);
		
		WebElement CLanguage = driver.findElement(By.id("invorg_os_officiallanguagetocommunicate"));
		CLanguage.click();
		Thread.sleep(3000);

		Select SelectCLanguage = new Select(driver.findElement(By.id("invorg_os_officiallanguagetocommunicate_i")));
		SelectCLanguage.selectByVisibleText("French");
		Thread.sleep(3000);
		
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		
		Alert popup = driver.switchTo().alert();
		popup.accept();
		Thread.sleep(3000);
		
	}
	
}
