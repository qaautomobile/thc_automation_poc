package Executional;


import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import Functional.CommonFunctions;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class AddClient_From_IntakeMainScreen {

	WebDriver driver = Login_Application.driver;
	
	@Test(priority=1)
	public void Click_On_Logo() throws SQLException, InterruptedException
	{	
		driver = Login_Application.driver;
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		
		driver.findElement(By.id("TabnavTabLogoTextId")).click();
		Thread.sleep(1500);
	}
	
	@Test(priority=2)
	public void Click_PlusIcon() throws InterruptedException
	{	
		driver.switchTo().frame(driver.findElement(By.id("contentIFrame2")));
		Thread.sleep(1000);
		
		driver.switchTo().frame(driver.findElement(By.id("dashboardFrame")));
		Thread.sleep(1000);
		
		driver.findElement(By.id("Component189ab11_addImageButton")).click();	
		Thread.sleep(2000);
		
		try {
			   Robot robot = new Robot();
			   robot.keyPress(KeyEvent.VK_ENTER);
			   robot.keyRelease(KeyEvent.VK_ENTER);
			   robot.delay(200);
			}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
	}

	@Test(priority=3)
	public void Verify_AddClientPage_Open() throws InterruptedException
	{
		
	}
}
