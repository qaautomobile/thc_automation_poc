package Executional;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.Test;

public class Login_Application {

	static WebDriver driver;

	String URL = "https://totalhealthcaredev1.onecaresupport.ca";
	String UserName = "tcss\\CCSDP.user2";
	String Password = "hawL@#c6%";
	public static SimpleDateFormat dateFormat ;
	public static String FileName;
	public static File file1;

	@Test(priority=1)
	public void OpenBrowser() throws FileNotFoundException 
	{
		Date date = new Date() ;
		dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
		FileName = "Execution_"+dateFormat.format(date); 
		
		file1 = new File(System.getProperty("user.dir")+"\\ResultFolder\\"+FileName+".txt") ;

		FileOutputStream fos = new FileOutputStream(file1);
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
		
		// start driver
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\RequiredFiles\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	}

	@Test(priority=2)
	public void Open_WebURL() throws InterruptedException
	{
		driver.get(URL);
		Thread.sleep(5000);
	}

	@Test(priority=3)
	public void Login() throws InterruptedException
	{
		// User ID
		WebElement User = driver.findElement(By.id("userNameInput"));
		User.clear();
		User.sendKeys(UserName);
		
		// Password
		WebElement Pass = driver.findElement(By.id("passwordInput"));
		Pass.clear();
		Pass.sendKeys(Password);
		
		// Login Button
		driver.findElement(By.id("submitButton")).click();
		
		Thread.sleep(12000);
		
		driver.switchTo().frame(driver.findElement(By.id("InlineDialog_Iframe")));
		Thread.sleep(2000);
		
		driver.findElement(By.id("butBegin")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
	}
}
