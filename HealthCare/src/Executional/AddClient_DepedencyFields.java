package Executional;


import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import Functional.CommonFunctions;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class AddClient_DepedencyFields {

	WebDriver driver;
	public static SimpleDateFormat dateFormat ;
	public static String FileName;
	
	String Database = "health";
	String TestTable = "totalhealthdata";
	String DBUser = "localhost";
	String DBPass = "Test@1234";
	
	
	static String FirstName,NickName,MiddleName;
	static String LastName;
	static String DateOfBirth,BirthType,DateOfBirthCurrent,DateOfBirthFuture;
	static String HealthCardNumber;
	static String SelectIdentifyAs;;
	static String PrimaryLanguage,AutomationNote,MoreInformation;
	static String CommunicationLanguage;
	static String ClientGender;
	static String ClientMaritalStatus,HowHearAboutUs;
	static String Version;
	static String CardExpiry;
	static String MainPhone, MainPhoneExt, Mobile, SecondaryPhone, SecondaryPhoneExt, Fax,SecondaryLanguage; 
	
	@Test(priority=1)
	public void Collect_RequiredData() throws SQLException
	{	
		//*****************************
		/*Connection Conn = CommonFunctions.OpenConnection(DBUser,DBPass);
		Statement stmt=Conn.createStatement();  
		ResultSet rs=stmt.executeQuery("CaseNo, CaseName , UserName ,Password ,FirstName ,LastName ,DateOfBirth ," +
				"HealthCardNumber ,IdentifyAs ,PrimaryLang ,CommunicationLang ,Gender ,MaritalStatus,Version,CardExpiry,SecondaryLanguage  "
				+ " FROM "+TestTable+" '");  
		
		while(rs.next())  
		{
			
			
			FirstName = rs.getString(1);
			NickName = "Automation_NickName";
			MiddleName = "Automation_MiddleName";
			LastName = rs.getString(2);
			DateOfBirth = rs.getString(3);
			BirthType = "Actual";
			DateOfBirthCurrent = "12/17/2018";
			DateOfBirthFuture = "12/10/2020";
			HealthCardNumber = rs.getString(4);
			SelectIdentifyAs = rs.getString(5);
			AutomationNote = "Automation Note";
			HowHearAboutUs = "Internet";
			MoreInformation = "Automation_Information";
			PrimaryLanguage = rs.getString(6);
			CommunicationLanguage = rs.getString(7);
			ClientGender = rs.getString(8);
			ClientMaritalStatus = rs.getString(9);
			Version = rs.getString(10);
			CardExpiry = rs.getString(11);
			MainPhone = rs.getString(11);
			MainPhoneExt = rs.getString(11);
			Mobile = rs.getString(11);
			SecondaryPhone = rs.getString(11);
			SecondaryPhoneExt = rs.getString(11);
			Fax = rs.getString(11);
			SecondaryLanguage = rs.getString(12);
			
		}*/
		//**********************************
		
			driver = Login_Application.driver;
			//#####################
			FirstName = "AUTOMATION_CLIENT_ 2";
			NickName = "Automation_NickName";
			MiddleName = "Automation_MiddleName";
			LastName = "LAST_1";
			DateOfBirth = "9/13/1990";
			BirthType = "Actual";
			DateOfBirthCurrent = "12/17/2018";
			DateOfBirthFuture = "12/10/2020";
			HealthCardNumber = "1234565447";
			SelectIdentifyAs = "Metis";
			AutomationNote = "Automation Note";
			HowHearAboutUs = "Internet";
			MoreInformation = "Automation_Information";
			PrimaryLanguage = "English";
			CommunicationLanguage = "English";
			ClientGender = "Male";
			ClientMaritalStatus = "Married";
			Version = "AB";
			CardExpiry = "10/10/2020";
			MainPhone = "1234569879";
			MainPhoneExt = "123654";
			Mobile = "1122334455";
			SecondaryPhone = "9876541230";
			SecondaryPhoneExt = "321654";
			Fax = "2233445566";
			SecondaryLanguage = "Arabic";
			//#########################
	}
	
	@Test(priority=3)
	public void Open_IntakeOptions() throws InterruptedException
	{	
		// Open Intak option
		driver.findElement(By.cssSelector("#TabIntake")).click();	
		Thread.sleep(2000);
	}

	@Test(priority=4)
	public void Open_ClientPage() throws InterruptedException
	{
		// Select Client from popup
		driver.findElement(By.cssSelector("#nav_conts")).click();
		Thread.sleep(20000);
	}

	@Test(priority=5)
	public void Add_NewClient() throws InterruptedException
	{
		// Click on New button
		driver.findElement(By.xpath(".//*[@id='contact|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.contact.NewRecord']")).click();
		Thread.sleep(40000);
		
		try {
			   Robot robot = new Robot();
			   robot.keyPress(KeyEvent.VK_ENTER);
			   robot.keyRelease(KeyEvent.VK_ENTER);
			   robot.delay(200);
			}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		
		Thread.sleep(20000);
	}
	
	@Test(priority=6)
	public void Open_NewClientEntryPage() throws InterruptedException
	{
		// switch frame
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		driver.switchTo().frame(driver.findElement(By.id("contentIFrame1")));
		Thread.sleep(2000);
		driver.switchTo().frame(driver.findElement(By.id("WebResource_sectiontabs1")));
		Thread.sleep(2000);

		// open client details tab
		driver.findElement(By.xpath(".//*[@id='CLIENTDETAILS_TAB']/a")).click();
		Thread.sleep(5000);
		
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.id("contentIFrame1")));
		Thread.sleep(3000);
	}

	@Test(priority = 7)
	public void Client_Checkbox()
	{
		WebElement Checbox = driver.findElement(By.id("invorg_bool_client"));
		if(Checbox.getAttribute("isinlinecheckbox").equalsIgnoreCase("True"))
		{
			System.out.println("PASS,Client Checkbox is selected");
		}
		else
		{
			System.out.println("FAIL,Client Checkbox is NOT selected");
		}
	}
		
	@Test(priority=7)
	public void NewClinet_FirstName() throws InterruptedException
	{
		WebElement FName = driver.findElement(By.id("firstname"));
		FName.sendKeys(FirstName);
		Thread.sleep(2000);
	}
	
	@Test(priority = 8)
	public void NewClient_Salutation() throws InterruptedException
	{
		WebElement Salutation = driver.findElement(By.id("invorg_os_salutation"));
		Salutation.click();
		Thread.sleep(1000);
		
		Select SelectSalutation = new Select(driver.findElement(By.id("invorg_os_salutation_i")));
		SelectSalutation.selectByVisibleText("Mr.");
	}
	
	@Test(priority=9)
	public void NewClinet_NickName() throws InterruptedException
	{
		WebElement FName = driver.findElement(By.id("nickname"));
		FName.sendKeys(NickName);
		Thread.sleep(2000);
	}
	
	@Test(priority=10)
	public void NewClinet_MiddleName() throws InterruptedException
	{
		WebElement FName = driver.findElement(By.id("middlename"));
		FName.sendKeys(MiddleName);
		Thread.sleep(2000);
	}		

	@Test(priority=11)
	public void NewClinet_LastName() throws InterruptedException
	{
		WebElement LName = driver.findElement(By.id("lastname"));
		LName.sendKeys(LastName);
		Thread.sleep(2000);
	}

	@Test(priority=12) 
	public void NewClinet_BirthDate_CurrentDate()throws InterruptedException
	{
		WebElement Bdate = driver.findElement(By.id("birthdate"));
		Bdate.sendKeys(DateOfBirthCurrent);
		Thread.sleep(2000);
		Bdate.sendKeys(Keys.TAB);
		
		//Verify Error message
		try
		{
			WebElement BdateErrorSymbol = driver.findElement(By.cssSelector("#birthdate .ms-crm-Inline-HasError"));
			if(BdateErrorSymbol.isEnabled())
				System.out.println("PASS, Error Symbol is displaying on the page :");
		}
		catch (Exception ex) {
			System.out.println("FAIL, Error Symbol is displaying on the page :");
		}
		
	}
	
	@Test(priority=13) 
	public void NewClinet_BirthDate_FutureDate()throws InterruptedException
	{
		
		WebElement Bdate = driver.findElement(By.id("birthdate"));
		Bdate.sendKeys(Keys.SHIFT,Keys.TAB);
		Bdate.sendKeys(DateOfBirthFuture);
		Thread.sleep(2000);
		Bdate.sendKeys(Keys.TAB);
		
		//Verify Error message
		try
		{
			WebElement BdateErrorSymbol = driver.findElement(By.cssSelector("#birthdate .ms-crm-Inline-HasError"));
			if(BdateErrorSymbol.isEnabled())
				System.out.println("PASS, Error Symbol is displaying on the page :");
		}
		catch (Exception ex) {
			System.out.println("FAIL, Error Symbol is displaying on the page :");
		}
	}
	
	@Test(priority=14) 
	public void NewClinet_BirthDate()throws InterruptedException
	{
		WebElement Bdate = driver.findElement(By.id("birthdate"));
		Bdate.sendKeys(Keys.SHIFT,Keys.TAB);
		Bdate.sendKeys(DateOfBirth);
		Thread.sleep(2000);
	}

	@Test(priority=15) 
	public void NewClinet_DateOfBirthType()throws InterruptedException
	{
		driver.findElement(By.id("birthdate")).sendKeys(Keys.SHIFT);
		
		Select SelectBType = new Select(driver.findElement(By.id("invorg_os_birthdatetype_i")));
		SelectBType.selectByVisibleText(BirthType);
	}
	
	@Test(priority=16)
	public void NewClinet_HealthCardNumber() throws InterruptedException
	{
		try
		{
			WebElement HealthCardNo = driver.findElement(By.id("invorg_slot_healthcardnumber"));
			Thread.sleep(1000);
			HealthCardNo.sendKeys(HealthCardNumber);
			Thread.sleep(2000);
			
		}
		catch (Exception ex) {
			System.out.println(ex);
		}
	}

	@Test(priority=17)
	public void NewClinet_Versioncode() throws InterruptedException
	{
		
		WebElement Versioncode = driver.findElement(By.id("invorg_slot_healthcardversioncode"));
		Versioncode.sendKeys(Version);
		Thread.sleep(2000);
		
	}
	
	@Test(priority=18)
	public void NewClinet_HealthCardExpiry() throws InterruptedException
	{
		
		WebElement HealthCardExpiry = driver.findElement(By.id("invorg_dt_healthcardexpirydate"));
		HealthCardExpiry.sendKeys(CardExpiry);
		Thread.sleep(3000);
		
		HealthCardExpiry.sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		driver.findElement(By.id("invorg_os_nohealthcardreason")).sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		Select Gender = new Select(driver.findElement(By.id("gendercode_i")));
		Gender.selectByVisibleText("Male");
		Thread.sleep(2000);
		
	}
	
	@Test(priority=19)
	public void NewClinet_MaritalStatus() throws InterruptedException
	{
		WebElement MaritalStatus = driver.findElement(By.id("familystatuscode"));
		MaritalStatus.click();
		Thread.sleep(3000);
		
		Select MaritalCode = new Select(driver.findElement(By.id("familystatuscode_i")));
		MaritalCode.selectByVisibleText(ClientMaritalStatus);
		Thread.sleep(3000);
	}
	
	@Test(priority=20) 
	public void NewClinet_IdentifyAs() throws InterruptedException
	{
		WebElement DoUIdentify = driver.findElement(By.id("invorg_os_doyouidentifyasfirstnationmetis"));
		DoUIdentify.click();
		Thread.sleep(3000);
		
		Select SelectDoUIdentify = new Select(driver.findElement(By.id("invorg_os_doyouidentifyasfirstnationmetis_i")));
		SelectDoUIdentify.selectByVisibleText(SelectIdentifyAs);
		Thread.sleep(3000);
	}

	@Test(priority=21)
	public void NewClient_Note() throws InterruptedException
	{
		WebElement Note = driver.findElement(By.id("description"));
		Note.sendKeys(AutomationNote);
		Thread.sleep(2000);
	}
	
	@Test(priority=22)
	public void NewClinet_HowDidYouHearAboutUs() throws InterruptedException
	{
		
		WebElement HearAboutUs = driver.findElement(By.id("invorg_os_howdidyouhearaboutus"));
		HearAboutUs.click();
		Thread.sleep(3000);
		
		Select SelectHearAboutUs = new Select(driver.findElement(By.id("invorg_os_howdidyouhearaboutus_i")));
		SelectHearAboutUs.selectByVisibleText(HowHearAboutUs);
		Thread.sleep(3000);
	}
	
	@Test(priority=23)
	public void NewClient_MoreInformation() throws InterruptedException
	{
		WebElement Info = driver.findElement(By.id("invorg_slot_issafetocontactdirectlycomment"));
		Info.sendKeys(MoreInformation);
		Thread.sleep(2000);
	}
	
	@Test(priority=24)
	public void NewClient_MainPhone() throws InterruptedException
	{
		WebElement mainPhone = driver.findElement(By.id("telephone1"));
		mainPhone.sendKeys(MainPhone);
		Thread.sleep(2000);
	}

	@Test(priority=25)
	public void NewClient_MainPhoneExt() throws InterruptedException
	{
		WebElement mainPhoneExt = driver.findElement(By.id("invorg_slot_telephone1ext"));
		mainPhoneExt.sendKeys(MainPhoneExt);
		Thread.sleep(2000);
	}
	
	@Test(priority=26)
	public void NewClient_MainMobile() throws InterruptedException
	{
		WebElement mainMobile = driver.findElement(By.id("mobilephone"));
		mainMobile.sendKeys(Mobile);
		Thread.sleep(2000);
	}
	
	@Test(priority=27)
	public void NewClient_SecondaryNumber() throws InterruptedException
	{
		WebElement secondryNo = driver.findElement(By.id("telephone2"));
		secondryNo.sendKeys(SecondaryPhone);
		Thread.sleep(2000);
	}
	
	@Test(priority=28)
	public void NewClient_SecondaryNumbExt() throws InterruptedException
	{
		WebElement secondryNoExt = driver.findElement(By.id("invorg_slot_telephone2ext"));
		secondryNoExt.sendKeys(SecondaryPhoneExt);
		Thread.sleep(2000);
	}

	@Test(priority=29)
	public void NewClient_Fax() throws InterruptedException
	{
		WebElement faxnumber = driver.findElement(By.id("fax"));
		faxnumber.sendKeys(Fax);
		Thread.sleep(2000);
		
		WebElement nextbutton = driver.findElement(By.id("invorg_bool_cansendcommunicationemail"));
		nextbutton.sendKeys(Keys.TAB);
	}
	
	@Test(priority=30)
	public void NewClinet_PrimaryLanguage() throws InterruptedException
	{
		WebElement PLanguage = driver.findElement(By.id("invorg_os_primarycommunicationlanguage"));
		PLanguage.click();
		Thread.sleep(3000);
		
		Select SelectPLanguage = new Select(driver.findElement(By.id("invorg_os_primarycommunicationlanguage_i")));
		SelectPLanguage.selectByVisibleText(PrimaryLanguage);
		Thread.sleep(3000);
	}

	@Test(priority=31)
	public void NewClinet_SecondaryLanguage() throws InterruptedException
	{
		WebElement SLanguage = driver.findElement(By.id("invorg_os_secondarycommunicationlanguage"));
		SLanguage.click();
		Thread.sleep(3000);
		
		Select SelectPLanguage = new Select(driver.findElement(By.id("invorg_os_secondarycommunicationlanguage_i")));
		SelectPLanguage.selectByVisibleText(SecondaryLanguage);
		Thread.sleep(3000);
		
		SLanguage.sendKeys(Keys.TAB);
	}
		
	@Test(priority=32)
	public void NewClinet_CommunicationLanguage() throws InterruptedException
	{
		WebElement CLanguage = driver.findElement(By.id("invorg_os_officiallanguagetocommunicate"));
		CLanguage.click();
		Thread.sleep(3000);

		Select SelectCLanguage = new Select(driver.findElement(By.id("invorg_os_officiallanguagetocommunicate_i")));
		SelectCLanguage.selectByVisibleText(CommunicationLanguage);
		Thread.sleep(3000);
		
		
	}
	
	@Test(priority=32) 
	public void NewClinet_SaveButton() throws InterruptedException
	{
		driver.switchTo().defaultContent();
		WebElement Save = driver.findElement(By.xpath(".//*[@id='contact|NoRelationship|Form|Mscrm.Form.contact.Save']"));
		Save.click();
		Thread.sleep(25000);
	}
}
