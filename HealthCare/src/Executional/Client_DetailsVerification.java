package Executional;

//import org.openqa.jetty.http.handler.ExpiryHandler;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Functional.CommonFunctions;


public class Client_DetailsVerification {

	WebDriver driver;

	 String FirstName,NickName,MiddleName;
	 String LastName;
	 String DateOfBirth,BirthType,DateOfBirthCurrent,DateOfBirthFuture;
	 String HealthCardNumber;
	 String SelectIdentifyAs;;
	 String PrimaryLanguage,AutomationNote,MoreInformation;
	 String CommunicationLanguage;
	 String ClientGender;
	 String ClientMaritalStatus,HowHearAboutUs;
	 String Version;
	 String CardExpiry;
	 String MainPhone, MainPhoneExt, Mobile, SecondaryPhone, SecondaryPhoneExt, Fax,SecondaryLanguage;
	
	@Test(priority=1)
	public void Collect_RequiredData()
	{
		driver = Login_Application.driver;
		FirstName = AddClient.FirstName;
		LastName = AddClient.LastName;
		DateOfBirth = AddClient.DateOfBirth;
		DateOfBirthCurrent = AddClient.DateOfBirthCurrent;
		DateOfBirthFuture = AddClient.DateOfBirthFuture;
		HealthCardNumber = AddClient.HealthCardNumber;
		SelectIdentifyAs = AddClient.SelectIdentifyAs;
		PrimaryLanguage = AddClient.PrimaryLanguage;
		CommunicationLanguage = AddClient.CommunicationLanguage;
		ClientGender = AddClient.ClientGender;
		ClientMaritalStatus = AddClient.ClientMaritalStatus;
		Version = AddClient.Version;
		CardExpiry = AddClient.CardExpiry;
		AutomationNote = AddClient.AutomationNote;
		HowHearAboutUs = AddClient.HowHearAboutUs;
		MoreInformation = AddClient.MoreInformation;
		MainPhone = AddClient.MainPhone;
		MainPhoneExt = AddClient.MainPhoneExt;
		Mobile = AddClient.Mobile;
		SecondaryPhone = AddClient.SecondaryPhone;
		SecondaryPhoneExt = AddClient.SecondaryPhoneExt;
		Fax = AddClient.Fax;
		SecondaryLanguage = AddClient.SecondaryLanguage;
		
		String MainPhone1 = MainPhone.substring(0, 4);
		String MainPhone2 = MainPhone.substring(4, 7);
		String MainPhone3 = MainPhone.substring(7, 10);
		MainPhone = MainPhone1+"-"+MainPhone2+"-"+MainPhone3;
		
		String MainPhoneExt1 = MainPhoneExt.substring(0, 3);
		String MainPhoneExt2 = MainPhoneExt.substring(3, 6);
		MainPhoneExt = MainPhoneExt1+"-"+MainPhoneExt2;
		
		String Mobile1 = Mobile.substring(0, 4);
		String Mobile2 = Mobile.substring(4, 7);
		String Mobile3 = Mobile.substring(7, 10);
		Mobile = Mobile1+"-"+Mobile2+"-"+Mobile3;
		
		String SecondaryPhone1 = SecondaryPhone.substring(0, 4);
		String SecondaryPhone2 = SecondaryPhone.substring(4, 7);
		String SecondaryPhone3 = SecondaryPhone.substring(7, 10);
		SecondaryPhone = SecondaryPhone1+"-"+SecondaryPhone2+"-"+SecondaryPhone3;
		
		String SecondaryPhoneExt1 = SecondaryPhoneExt.substring(0, 3);
		String SecondaryPhoneExt2 = SecondaryPhoneExt.substring(3, 6);
		SecondaryPhoneExt = SecondaryPhoneExt1+"-"+SecondaryPhoneExt2;
		
		String Fax1 = Fax.substring(0, 3);
		String Fax2 = Fax.substring(3, 6);
		Fax = Fax1+"-"+Fax2;
	}
	
	@Test(priority=3)
	public void Open_ClientOptions() throws InterruptedException
	{	
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		// Open Client option
		driver.findElement(By.cssSelector("#Tabnav_conts")).click();	
		Thread.sleep(6000);
	}

	@Test(priority=4)
	public void Open_ClientPage() throws InterruptedException
	{
		driver.findElement(By.cssSelector("#secondTabActionListFlyoutControl_scrollableContainer .nav-section:nth-child(1) li:nth-child(1)")).click();
		Thread.sleep(25000);
	}

	@Test(priority=5)
	public void Verify_FirstName() throws InterruptedException
	{
		driver.switchTo().frame(driver.findElement(By.id("contentIFrame1")));
		Thread.sleep(2000);
		// Click on New button
		String FName = driver.findElement(By.cssSelector("#firstname .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(FirstName, FName, "First Name : ");
		Thread.sleep(2000);
	}
	
	@Test(priority=6) //--
	public void Verify_NickName() throws InterruptedException
	{
		String NName = driver.findElement(By.cssSelector("#nickname .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(NickName, NName, "Nick Name : ");
		Thread.sleep(2000);
	}
	
	@Test(priority=7) //--
	public void Verify_MiddleName() throws InterruptedException
	{
		String MName = driver.findElement(By.cssSelector("#middlename .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(MiddleName, MName, "Middle Name : ");
		Thread.sleep(2000);
	}
	
	
	@Test(priority=8)
	public void Verify_LastName() throws InterruptedException
	{
		String LName = driver.findElement(By.cssSelector("#lastname .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(LastName, LName, "Last Name : ");
		Thread.sleep(2000);
	}

	@Test(priority=9)
	public void Varify_BirthDate() throws InterruptedException
	{
		String Bdate = driver.findElement(By.cssSelector("#birthdate .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(DateOfBirth, Bdate, "Birth Date : ");
		Thread.sleep(2000);
	}

	@Test(priority=10)
	public void Varify_BirthType() throws InterruptedException
	{
		String Bdate = driver.findElement(By.cssSelector("#birthdate .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(DateOfBirth, Bdate, "Birth Date : ");
		Thread.sleep(2000);
	}
	
	@Test(priority=11)
	public void Varify_HealthCardNumber() throws InterruptedException
	{
		String HealthCardNo = driver.findElement(By.cssSelector("#invorg_slot_healthcardnumber .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(HealthCardNumber, HealthCardNo, "Health Card Number : ");
		Thread.sleep(2000);
	}

	@Test(priority=12)
	public void Varify_Versioncode() throws InterruptedException
	{
		String VersioCode = driver.findElement(By.cssSelector("#invorg_slot_healthcardversioncode .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(Version, VersioCode, "Version Code : ");
		Thread.sleep(2000);
	}
	
	@Test(priority=13)
	public void Varify_HealthCardExpiryDate() throws InterruptedException
	{
		String ExpiryDate = driver.findElement(By.cssSelector("#invorg_dt_healthcardexpirydate .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(CardExpiry, ExpiryDate, "Health Card Expiry Date : ");
		Thread.sleep(2000);
	}
	
	@Test(priority=14)
	public void Varify_Gender() throws InterruptedException
	{
		String Gender = driver.findElement(By.cssSelector("#gendercode .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(ClientGender, Gender, "Client Gender : ");
		Thread.sleep(2000);
	}
	
	@Test(priority=15)
	public void Varify_MaritalStatus() throws InterruptedException
	{
		String MaritalStatus = driver.findElement(By.cssSelector("#familystatuscode .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(ClientMaritalStatus, MaritalStatus, "Marital Status : ");
		Thread.sleep(2000);
	}
	
	@Test(priority=16)
	public void Varify_IdentifyAs() throws InterruptedException
	{
		String DoUIdentify = driver.findElement(By.cssSelector("#invorg_os_doyouidentifyasfirstnationmetis .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(SelectIdentifyAs, DoUIdentify, "Do you Identify As : " );
		Thread.sleep(2000);
	}

	@Test(priority=17) //
	public void Varify_Note() throws InterruptedException
	{
		String VerifyNote = driver.findElement(By.cssSelector("#description .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(AutomationNote, VerifyNote, "Client Note : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=18) //
	public void Varify_HowDidYouHearAboutUs() throws InterruptedException
	{
		String HearAboutUs = driver.findElement(By.cssSelector("#invorg_os_howdidyouhearaboutus .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(HowHearAboutUs, HearAboutUs, "Do you Identify As : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=19) //
	public void Varify_MoreInformation() throws InterruptedException
	{
		String MoreInfo = driver.findElement(By.cssSelector("#invorg_slot_issafetocontactdirectlycomment .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(MoreInformation, MoreInfo, "More Information : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=20) //
	public void Varify_MainNumber() throws InterruptedException
	{
		String DoUIdentify = driver.findElement(By.cssSelector("#telephone1 .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(MainPhone, DoUIdentify, "Main Mobile Number : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=21) //
	public void Varify_MainNumberExt() throws InterruptedException
	{
		String MainNum = driver.findElement(By.cssSelector("#invorg_slot_telephone1ext .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(MainPhoneExt, MainNum, "Main Phone Ext : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=22) //
	public void Varify_MainMobile() throws InterruptedException
	{
		String MainMobile = driver.findElement(By.cssSelector("#mobilephone .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(Mobile, MainMobile, "Main Mobile Number : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=23) //
	public void Varify_SecondaryNumber() throws InterruptedException
	{
		String SecNumber = driver.findElement(By.cssSelector("#telephone2 .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(SecondaryPhone, SecNumber, "Secondary Number : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=24) //
	public void Varify_SecondaryNumberExt() throws InterruptedException
	{
		String SecNoExt = driver.findElement(By.cssSelector("#invorg_slot_telephone2ext .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(SecondaryPhoneExt, SecNoExt, "Secondary Number Ext : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=25) //
	public void Varify_Fax() throws InterruptedException
	{
		String Faxx = driver.findElement(By.cssSelector("#fax .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(Fax, Faxx, "Fax : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=26)
	public void Varify_PrimaryLanguage() throws InterruptedException
	{
		String SelectPLanguage = driver.findElement(By.cssSelector("#invorg_os_primarycommunicationlanguage .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(PrimaryLanguage, SelectPLanguage, "Primary Language : ");
		Thread.sleep(2000);
	}

	@Test(priority=27) //
	public void Varify_SecondaryLanguage() throws InterruptedException
	{
		String SecLang = driver.findElement(By.cssSelector("#invorg_os_secondarycommunicationlanguage .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(SecondaryLanguage, SecLang, "Secondary Language : " );
		Thread.sleep(2000);
	}
	
	@Test(priority=28)
	public void Varify_CommunicationLanguage() throws InterruptedException
	{
		String CLanguage = driver.findElement(By.cssSelector("#invorg_os_officiallanguagetocommunicate .ms-crm-Inline-Value")).getText();
		CommonFunctions.Value_Verification(CommunicationLanguage, CLanguage, "Communication Language : ");
		Thread.sleep(2000);
	}

}
