package Executional;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Functional.CommonFunctions;


public class Logout {

	WebDriver driver;
	
	@Test(priority=1)
	public void Logout_Application()
	{
		driver = Login_Application.driver;
		driver.switchTo().defaultContent();
	}
	
	@Test(priority=2)
	public void Click_Profile() throws InterruptedException
	{
		driver.findElement(By.id("navBarUserInfoTextId")).click();
		Thread.sleep(2000);
	}
	
	@Test(priority=3)
	public void Logout_button() throws InterruptedException
	{
		driver.findElement(By.id("navTabButtonUserInfoSignOutId")).click();
		Thread.sleep(2000);
	}
	
	@Test(priority=4)
	public void Close_Browser() throws InterruptedException
	{	
		driver.quit();
	}

}
