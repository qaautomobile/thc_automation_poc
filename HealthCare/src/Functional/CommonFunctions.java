package Functional;

import org.testng.Assert;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;


public class CommonFunctions {
	
	static String Database = "test";
	static String TEST_XLSFilePath = System.getProperty("user.dir")+"\\TestData\\HealthData.xls";

	static String Databaseconnection = "localhost";
	static String DatabasePort = "3306";
	static int WorksheetNo;
	
	public static void Value_Verification(String Expected, String Actual, String VariableName)
	{
		if(Expected.equalsIgnoreCase(Actual))
		{
			System.out.println("PASS,"+VariableName+" Expected: "+Expected+" Actual: "+Actual);
		}
		else
		{
			System.out.println("FAIL,"+VariableName+" Expected: "+Expected+" Actual: "+Actual);
			Assert.fail();
		}
	}

	public static Connection OpenConnection(String UserName,String Password)
	{
		Connection Conn = null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");  
//			Conn=DriverManager.getConnection("jdbc:mysql://"+Databaseconnection+":"+DatabasePort+"",UserName,Password);	
			Conn=DriverManager.getConnection("jdbc:mysql://"+Databaseconnection+":"+DatabasePort+"",UserName,Password);
		}
		 catch(Exception ex)
		{
			 System.out.println(ex);
		}
		return Conn;
	}
}
